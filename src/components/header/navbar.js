import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => (
  <header className="fixed-top">
    <nav className="navbar navbar-expand-md navbar-dark  container">
      <a className="navbar-brand" ><img src="/image/eazy_my.jpg" alt="eazy my" /></a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"><i className="fas fa-bars"></i></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav mr-auto col-8">
          <li className="nav-item active col">
            <Link to='/' className="nav-link">Categories</Link>
          </li>
          <li className="nav-item col">
            <Link to='/notification' className="nav-link">Notification</Link>
          </li>
          <li className="nav-item col">
            <Link to='/login' className="nav-link">Login/Sign up</Link>
          </li>
          <li className="nav-item col">
            <Link to='/help' className="nav-link">Help</Link>
          </li>
        </ul>
      </div>
    </nav>
  </header>
)

export default Navbar;