import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addListItem } from '../../../store/action/action';
import getServices from '../../servicesLink/servicesLink';



class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            productList:'list', 
        };
    }

    

    _changeData() {
        //console.log('event called');
        this.props.addListItem(this.state.items);
    }

    componentDidMount() {
        if (this.props.listitem.length === 0) {
            fetch(getServices.serviecUrl+this.state.productList)
                .then(res => res.json())
                .then(
                    (result) => {
                        //console.log(result);
                        this.setState({
                            isLoaded: true,
                            items: result.data
                        });
                        this._changeData();
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        } else {
            this.setState({
                items: this.props.listitem,
                isLoaded: true,
            })
        }
    }

    selectedItem(item) {
       // console.log(item);
    }

    list_add() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                
                <div className='card-deck d-flex flex-wrap justify-content-center'>
                    {items.map(item => (
                        <div className="mb-4 list_card rounded" key={item.id} >
                            <Link to={`/product/${item.id}`} >
                                <img src={item.attributes.links.image} className="card-img-top" alt={item.attributes.title} />
                                <div className="card-body">
                                    <h5 className="card-title">{item.attributes.title}</h5>
                                    <b className="card-price">{item.attributes.price}</b>
                                </div>
                            </Link>
                        </div>

                    ))}
                </div>
            );
        }
    }


    render() {
        return (

            <div className="container pt-4">
                <h2 className="page-heading">LISTING</h2>
                {this.list_add()}
            </div>
        )
    }
}



// redux state prop and dispatch prop
function mapStateToProp(state) {
    return ({
        listitem: state.root.listitem
    })
}

function mapDispatchToProp(dispatch) {
    return ({
        addListItem: (list) => { dispatch(addListItem(list)) }
    })
}

export default connect(mapStateToProp, mapDispatchToProp)(ProductList);





