import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addListItem } from '../../../store/action/action';
import getServices from '../../servicesLink/servicesLink';



class ProductView extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            error: null,
            isLoaded: false,
            items: [],
            item: [],
            similar: [],
            productList:'list',
            productSimilar:'similar/',
        };
    }

    // store local data 
    _changeData() {
        //console.log('event called');
        this.props.addListItem(this.state.items);
    }

    componentDidMount() {
        //Get product list if I dont't have on redux else get on redux 
        const getindex = this.props.match.params.id - 1;
        if (this.props.listitem.length === 0) {
            fetch(getServices.serviecUrl+this.state.productList)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            items: result.data,
                            item: result.data[getindex],
                        });
                        this._changeData();
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        } else {
            this.setState({
                item: this.props.listitem[getindex],
                isLoaded: true,
            })
            //console.log(this.props.listitem[getindex]);
        }

        //get similar id for similar method
        this.similar(this.props.match.params.id);
    }

    // get data similar product 
    similar(id) {
        fetch(getServices.serviecUrl + this.state.productSimilar + id)
            .then(res => res.json())
            .then(
                (result) => {
                    //console.log(result);
                    this.setState({
                        isLoaded: true,
                        similar: result.data,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                });
    }

    //similar product HTML 
    similar_product() {
        const { error, isLoaded, similar } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="row">
                    <div className="col-12 similar">
                        <div className='card-deck mb-3 d-flex flex-wrap justify-content-left'>
                            <h3>SIMILAR ITEMS</h3>
                            {similar.map(item => (
                                <div className="mb-4 list_card rounded" key={item.id}>
                                    <img src={item.attributes.image} className="card-img-top" alt={item.attributes.title} />
                                    <div className="card-body">
                                        <h5 className="card-title">{item.attributes.title}</h5>
                                        <b className="card-price">{item.attributes.price}</b>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            )
        }
    }

// product view html
    text() {
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className='row'>
                    <div className='col-12 prohead'>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb d-sm-none">
                                <li className="breadcrumb-item">Home</li><li className="breadcrumb-item">Electronics</li>
                                <li className="breadcrumb-item">Games & Console</li>
                                <li className="breadcrumb-item active" aria-current="page">{item.attributes.title}</li>
                            </ol>
                        </nav>

                        <h2>{item.attributes.title}</h2>
                    </div>

                    <div className="col-lg-8 col-md-8 proleft">
                        
                        <picture className="text-center mb-3">
                            <img src={item.attributes.links.image} className="img-fluid" alt={item.attributes.title} />
                        </picture>
                    </div>


                    <div className="col-lg-4 col-md-4">
                        <div className="proright">
                            <div className="pt-1 pr-3 pl-3">
                                <div className="row mb-2">
                                    <div className="col text-left"><span className="icon-text"><i className="far fa-heart"></i> Wishlist</span></div>
                                    <div className="col text-left"><span className="icon-text"><i className="fas fa-share-alt"></i> Share</span></div>
                                </div>

                                <dl className="row mb-0">
                                    <dt className="col-12">Price</dt>
                                    <dd className="col-12 price">{item.attributes.price}</dd>

                                    <dt className="col-12">Item condition</dt>
                                    <dd className="col-12">{item.attributes.condition}</dd>

                                    <dt className="col-12">Item location</dt>
                                    <dd className="col-12">{item.attributes.location}</dd>

                                    <dt className="col-12">Seller info</dt>
                                    <dd className="col-12">
                                        <ul className="list-unstyled mb-0">
                                            <li className="media user">
                                                <i className="fas fa-user-circle mr-3"></i>
                                                <div className="media-body">
                                                    <h5 className="mt-0 mb-1">{item.attributes.seller_name}</h5>
                                                    {item.attributes.seller_type}
                                                </div>
                                            </li>
                                        </ul>
                                    </dd>
                                </dl>
                            </div>
                            <hr />
                            <div className="pb-1 pr-3 pl-3 contact">
                                <dl className="row mb-0">
                                    <dt className="col-12 mb-2"><span className="d-none d-sm-block">Interested with the ad? Contact the seller</span></dt>
                                    <dd className="col-12 mb-0">
                                        <button type="button" className="btn btn-outline-danger text-left col-lg-12 col-sm-12 col-md-12 col-5"><i className="fas fa-phone-volume"></i> {item.attributes.phone}</button>
                                        <button type="button" className="btn btn-outline-danger text-left col-lg-12 col-sm-12 col-md-12 col-3"> <i className="fas fa-envelope"></i> Email</button>
                                        <button type="button" className="btn btn-outline-danger text-left col-lg-12 col-sm-12 col-md-12 col-3 mr-0"> <i className="fas fa-comments"></i> Chat</button>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>


                    <div className="col-lg-8 col-md-8 proleft bottom_line">
                        <h3 className="mb-3"><b>DESCRIPTION</b> <span className="icon-text float-right"><i className="fab fa-font-awesome-flag"></i> Report Ad</span></h3>
                        
                            {
                                
                                item.attributes.description.split("n1").map(function (item, i) {
                                    return (
                                        <p key={i}>
                                            {item}
                                        </p>
                                    )
                                })
                            }
                        
                    </div>
                </div>

            );
        }
    }

    // similar method call  
    render() {
        return (
            <div className="container pt-4">
                {this.text()}
                {this.similar_product()}
            </div>
        )
    }

}

// redux state prop and dispatch prop
function mapStateToProp(state) {
    return ({
        listitem: state.root.listitem
    })
}

function mapDispatchToProp(dispatch) {
    return ({
        addListItem: (list) => { dispatch(addListItem(list)) }
    })
}

export default connect(mapStateToProp, mapDispatchToProp)(ProductView);
