import ActionTypes from '../constant/constant';

const INITIAL_STATE = {
    listitem: [],
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
         case ActionTypes.LISTITEM:
            return ({
                ...state,
                listitem: action.payload
            })         
        default:
            return state;
    }

}