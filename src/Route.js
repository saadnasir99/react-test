import React, { Component } from 'react';
import { Route, Router } from 'react-router-dom';
import  ProductView  from './components/pages/product/productView';
import ProductList from './components/pages/product/productList';
import Notification from './components/pages/notification/notification';
import Login from './components/pages/login/login';
import Help from './components/pages/help/help';
import Navbar from './components/header/navbar';
import history from './History';


class Routers extends Component {
    render() {
        return (
            <Router history={history}>
                <div>
                    <Navbar />
                    <main role="main" className='headertop'>
                        <Route exact path="/" component={ProductList} />
                        <Route exact path="/notification" component={Notification} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/help" component={Help} />
                        <Route path="/product/:id" component={ProductView} />
                    </main>
                    
                </div>
            </Router>
        )
    }
}

export default Routers;